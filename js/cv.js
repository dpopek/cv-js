var cv = {
    "phone_number": "123 456 789",
    "email": "sdgdf@sdd.pl",
    "city": "Chwa",

    "education":[
        {
            "school": "codementors",
            "year": "2018"
        },
        {
            "school": "PG",
            "year": "2014"
        },
        {
            "school": "SWPS",
            "year": "2012"
        }
    ],
    "jobs":[
        {
            "employer": "Coral",
            "years": "2016-2017"
        },
        {
            "employer": "Cumulus",
            "years": "2012-2015"
        }
    ],
    "skills":[
        "Mistrz Javy", "Cokolwiek"
    ],
    "interests":[
        "Basket", "Rower", "Taniec"
    ]
}

function populateCv(container) {
    var container = document.getElementById(container);

    var article = document.createElement("article");
    container.appendChild(article)

    var title = createHeader("h2", "curriculum vitae");
    article.appendChild(title);

    var section = createSection("Podstawowe informacje");
    article.appendChild(section);

    var table = createBasicInfoTable();
    section.appendChild(table);

    var section = createSection("Edukacja");
    article.appendChild(section);

    var  table = createEducationTable();
    section.appendChild(table);

    var section = createSection("Doświadczenie zawodowe");
    article.appendChild(section);

    var table = createJobsTable();
    section.appendChild(table);

    var section = createSection("Umiejetności");
    article.appendChild(section);

    var list = createSkillsList();
    article.appendChild(list);

    var section = createSection("Zainteresowania");
    article.appendChild(section);

    var list = createInterestsList();
    article.appendChild(list);

}

function createHeader(h, text) {
    header = document.createElement("header");
    h = document.createElement(h);
    h.innerHTML = text;
    header.appendChild(h);
    return header;
}

function createSection(title) {
    var section = document.createElement("section");
    section.appendChild(createHeader("h3", title));
    return section;
}

function createBasicInfoTable() {
    var table = document.createElement("table");
    table.appendChild(createRowWithHeaderInColumn("Numer telefonu: ", cv.phone_number));
    table.appendChild(createRowWithHeaderInColumn("Email: ", cv.email))
    table.appendChild(createRowWithHeaderInColumn("Miejsce zamieszkania:", cv.city))
    return table;
}

function createRowWithHeaderInColumn(header, value) {
    var tr = document.createElement("tr");
    var th = document.createElement("th");
    var td = document.createElement("td");
    th.innerHTML = header;
    td.innerHTML = value;
    tr.appendChild(th);
    tr.appendChild(td);
    return tr;
}

function createEducationTable() {
    var tr = document.createElement("tr");
    table = document.createElement("table");

    tr = createHeaderRow("Uczelnia", "Rok");
    table.appendChild(tr);

    for (element in cv.education) {
        tr = createRegularRow(cv.education[element].school, cv.education[element].year);
        table.appendChild(tr);
    }
    return table;
}

function createJobsTable() {
    var tr = document.createElement("tr");
    table = document.createElement("table");

    tr = createHeaderRow("Robota", "Lata");
    table.appendChild(tr);

    for (element in cv.jobs) {
        tr = createRegularRow(cv.jobs[element].employer, cv.jobs[element].years);
        table.appendChild(tr);
    }
    return table;
}

function createRegularRow(value1, value2) {
    var tr = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");

    td1.innerHTML = value1;
    tr.appendChild(td1);
    td2.innerHTML = value2;
    tr.appendChild(td2);
    return tr;
}

function createHeaderRow(value1, value2) {
    var tr = document.createElement("tr");
    var th1 = document.createElement("th");
    var th2 = document.createElement("th");

    th1.innerHTML = value1;
    tr.appendChild(th1);
    th2.innerHTML = value2;
    tr.appendChild(th2);
    return tr;
}

function createSkillsList() {
    var list = document.createElement("ul");
    list.classList.add("listaumiejetnosci");
    for (item in cv.skills){
        var li = document.createElement("li");
        li.innerHTML = cv.skills[item];
        list.appendChild(li);
    }
    return list;
}

function createInterestsList() {
    var list = document.createElement("ul");
    list.classList.add("listazainteresowan");
    for (item in cv.interests){
        var li = document.createElement("li");
        li.innerHTML = cv.interests[item];
        list.appendChild(li);
    }
    return list;
}